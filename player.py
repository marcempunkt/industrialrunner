import pygame as pg
from settings import *

vec = pg.math.Vector2

class Player(pg.sprite.Sprite):

    def __init__(self, game):
        pg.sprite.Sprite.__init__(self)

        self.game = game

        self.image = pg.Surface((50, 100))
        self.image.fill((25, 25, 25))
        self.rect = self.image.get_rect()

        self.pos = vec( WIDTH/2, HEIGHT/2 )
        self.acc = vec(0, 0)
        self.vel = vec(0, 0)

        self.jumping = False

    def update(self):
        # Gravity
        self.acc = vec(0, PLAYER_GRAVITY)

        self.events()

        self.acc.x += self.vel.x * PLAYER_FRICTION
        self.vel += self.acc
        self.pos += self.vel + 0.5 * self.acc
        self.check_boundaries()
        self.rect.midbottom = self.pos

    def events(self):
        # Events
        keystate = pg.key.get_pressed()
        if keystate[pg.K_LEFT]:
            self.acc.x = -PLAYER_ACC
        if keystate[pg.K_RIGHT]:
            self.acc.x = PLAYER_ACC
        if keystate[pg.K_SPACE]:
            self.jump()

    def jump(self):
        if self.jumping:
            self.vel.y = -PLAYER_JUMP
            self.jumping = False

    def check_boundaries(self):
        # Bottom
        if self.pos.y > HEIGHT:
            self.pos.y = HEIGHT
            self.jumping = True
            self.acc = vec(0, 0)

        # Left and Right
        if self.pos.x < 0:
            self.pos.x = 0
        if self.pos.x > WIDTH:
            self.pos.x = WIDTH

        # Sprites
        self.check_collision()

    def check_collision(self):
        # Collision Player and Platform
        # +1 und nach collide -1 muss weil sonst
        # der Player auf der Platf. wackelt
        self.rect.y += 1
        hits = pg.sprite.spritecollide(self, self.game.all_platforms, False)
        self.rect.y -= 1
        if hits:
            self.pos.y = hits[0].rect.top
            self.vel.y = 0
            self.jumping = True
