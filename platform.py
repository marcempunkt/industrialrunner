import pygame as pg
import random

from settings import *

class Platform(pg.sprite.Sprite):

    def __init__(self):
        pg.sprite.Sprite.__init__(self)


        self.image = pg.Surface((100, 25))
        self.image.fill((15, 15, 15))
        self.rect = self.image.get_rect()

        self.rect.y = random.randrange(HEIGHT/2, HEIGHT)
        self.rect.x = WIDTH

    def update(self):
        self.rect.x -= 2
        if self.rect.right < 0:
            self.rect.y = random.randrange(HEIGHT/2, HEIGHT)
            self.rect.x = WIDTH
