import pygame as pg

from settings import *
from player import Player
from platform import Platform

class Game:

    def __init__(self):
        # Initialize Pygame and Game
        pg.init()
        pg.mixer.init()
        pg.display.set_caption(TITLE)
        self.screen = pg.display.set_mode((WIDTH, HEIGHT))
        self.clock = pg.time.Clock()
        self.running = True

    def run(self):
        # Run Game
        self.playing = True
        while self.playing:
            self.clock.tick(60)
            self.events()
            self.update()
            self.draw()

    def new(self):
        # Start a new game
        # init Sprite Groups
        self.all_sprites = pg.sprite.Group()
        self.all_platforms = pg.sprite.Group()

        # Create Player
        self.platform = Platform()
        self.player = Player(self)
        self.all_sprites.add(self.platform)
        self.all_platforms.add(self.platform)
        self.all_sprites.add(self.player)

        self.run()

    def update(self):
        # Game Loop - Update
        self.all_sprites.update()

    def draw(self):
        # Game Loop - Draw
        self.screen.fill(RED)
        self.all_sprites.draw(self.screen)
        pg.display.flip()

    def events(self):
        # Game Loop - Events
        for event in pg.event.get():
            if event.type == pg.QUIT:
                if self.playing:
                    self.playing = False
                self.running = False
            if event.type == pg.K_p:
                print("k pressed")

    def show_start_screen(self):
        pass

    def show_gameover_screen(self):
        pass






g = Game()
g.show_start_screen()
while g.running:
    g.new()
    g.show_gameover_screen()
pg.quit()
